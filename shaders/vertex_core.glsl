//GLSL version : GLSL is a shader language
#version 440

//On what order our buffer will be send to GPU

layout (location = 0) in vec3 vertex_position;

//What the GPU will send out to fragment GPU program
out vec3 vs_position;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

void main()
{
    //We change the position according to the ModelMatrix translations
    vs_position = vec4(ModelMatrix * vec4(vertex_position, 1.f)).xyz;

    //OpenGL final position for the point we entered
    gl_Position = ModelMatrix * ProjectionMatrix * ViewMatrix * vec4(vertex_position, 1.f);

}

