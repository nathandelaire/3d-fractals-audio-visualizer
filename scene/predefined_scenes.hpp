//
// Created by natha on 16/04/2021.
//

#ifndef RAYMARCHING_PREDEFINED_SCENES_HPP
#define RAYMARCHING_PREDEFINED_SCENES_HPP

#include "Scene.hpp"

Scene *scenePlanet = new Scene(PLANETS,
                               glm::vec3(1.f),
                               glm::vec3(0.f),
                               100.f,
                               0.01f,
                               100.f,
                               true,
                               30.f,
                               true,
                               true,
                               false,
                               false);

Scene *sceneMandelbulb = new Scene(MANDELBULB,
                                   glm::vec3(0.63f, 0.06f, 0.13f),
                                   glm::vec3(0.f, -0.f, -2.f),
                                  300.f,
                                   0.01f,
                                   100.f,
                                   true,
                                   30.f,
                                   false,
                                   false,
                                   false,
                                   false);

Scene *sceneMandelbox = new Scene(MANDELBOX,
                                  glm::vec3(0.87f, 0.72f, 0.54f),
                                  glm::vec3(0.f, 0.f, -30.f),
                                  200.f,
                                  0.005f,
                                  100.f,
                                  false,
                                  30.f,
                                  true,
                                  false,
                                  false,
                                  true);

Scene *sceneGyroid = new Scene(GYROID,
                               glm::vec3(0.86f, 0.12f, 0.06f),
                               glm::vec3(0.f),
                               300.f,
                               0.01f,
                               100.f,
                               true,
                               30.f,
                               true,
                               false,
                               true,
                               false);

Scene *sceneSponge = new Scene(SPONGE,
                               glm::vec3(0.54f, 0.81f, 0.87f),
                               glm::vec3(0.f, 0.f, -20.f),
                               300,
                               0.01f,
                               100.f,
                               true,
                               30.f,
                               true,
                               false,
                               false,
                               false);


#endif //RAYMARCHING_PREDEFINED_SCENES_HPP
