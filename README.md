# 3D Fractals Audio Visualizer

Starded from an already existing project I made "OpenGL Raymarched 3D Fractals", this one takes audio output as stream and send it in GPU so that the fractals moves along the audio output stream.

Useful for party ambiance or raves.

Made by Nathan DELAIRE.


# Requirements

# Installation

## Windows
I personnaly use MinGW in order to have a C and C++ compiler on my window 10 machine. So the option -G in the line will be "MinGW Makefiles".
Go in the projet folder then :
``
cmake  -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - MinGW Makefiles" .
``
``
cmake --build . --target Raymarching -- -j 12
``
A **Raymarching.exe** will appear on your current folder. It's the application.

## Linux

Because the project uses very low-level programming in order to get audio output stream, Windows has a very specific API for audio programming. Hence, this project is not compatible for Linux yet.

# How to use it


## Controls

Use your keyboard and your mouse to move around the world :
* **E** : Front
* **X** : Back
* **S** : Left
* **F** : Right
* **Space** : Up
* **C** : Down
* **Z** : While pressed, you sprint

