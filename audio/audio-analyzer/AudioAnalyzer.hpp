//
// Created by natha on 22/04/2021.
//

#pragma once
#include <SFML\Audio.hpp>

#include <complex>
#include <valarray>
#include <vector>
#include <stdlib.h>

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;

struct AudioAnalyzer {

    sf::Sound &sound;
    sf::SoundBuffer &buffer;
    long long sampleCount;
    int sampleRate;
    int channelCount;

    //this should be a power of 2
    int bufferSize = 2048;
    CArray complex;
    //Information when fft created
    double low;
    double mid;
    double high;

    //If the sound is from a music file (false) or a captured output audio file(true)
    bool capture;


    AudioAnalyzer(sf::Sound &sound, sf::SoundBuffer &buffer, bool capture) : sound(sound), buffer(buffer), capture(capture) {
        sampleCount = buffer.getSampleCount();
        channelCount = buffer.getChannelCount();
        sampleRate = buffer.getSampleRate() * channelCount;
        complex.resize(bufferSize);
    }

    //Fast Fourier Transform
    void fft(CArray &x) {
        const int N = x.size();
        if (N <= 1) return;
        CArray even = x[std::slice(0, N / 2, 2)];
        CArray odd = x[std::slice(1, N / 2, 2)];
        fft(even);
        fft(odd);

        for (int k = 0; k < N / 2; k++) {
            Complex t = std::polar(1.0, -2 * 3.141592653589 * k / N) * odd[k];
            x[k] = even[k] + t;
            x[k + N / 2] = even[k] - t;
        }
    }

    double getMagnitude(Complex complex){
        return std::sqrt(complex.real() * complex.real() + complex.imag() * complex.imag());
    }

    void update() {
        low = 0;
        mid = 0;
        high = 0;

        //On récupère la portion que l'on veut évaluer
        if(!capture)
        {
            int mark = sound.getPlayingOffset().asSeconds() * sampleRate;
            for (int i = mark; i < mark + (bufferSize) && i < sampleCount; ++i) {
                complex[i - mark] = Complex(buffer.getSamples()[i], 0.0);
            }
        }
        else{
            for (int i = 0; i < sampleCount; ++i) {
                complex[i] = Complex(buffer.getSamples()[i], 0.0);
            }
        }

        //analyse frequencies
        fft(complex);

        //Hz difference at each indices of the fft buffer
        int off = 47;

        /**
        std::vector<double> magnitudes;
        for(auto c : complex)
            magnitudes.push_back(getMagnitude(c));
        **/

        for (int i = 0; i < complex.size() / 2; i++) {
            //Magnitude it the amplitude at Hz "off" -> offset
            double magnitude = std::sqrt(complex[i].real() * complex[i].real() + complex[i].imag() * complex[i].imag());

            if (off < 189)
                low += magnitude;

            if (off > 282 && off < 2600)
                mid += magnitude;

            if(off > 4500)
                high += magnitude;

            off += 47;
        }
    }

    inline double getLow(int lowLevel) { return low / lowLevel;}
    inline double getMid(int midLevel) { return mid / midLevel;}
    inline double getHigh() { return high / 16900000;}


};