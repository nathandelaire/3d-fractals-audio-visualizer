//
// Created by natha on 04/06/2021.
//

#include "common.h"
#include <iostream>
#include <comdef.h>

#ifndef RAYMARCHING_MAIN_LOOPBACK_CAPTURE_HPP
#define RAYMARCHING_MAIN_LOOPBACK_CAPTURE_HPP

/**
 * Struct containing all the devices and their total size
 */
struct DEVICE_LIST{
    int count;
    const char** devices;
};

/**
 * Function processing all audio output recording / information getting
 *
 * @param argc : number of arguments
 * @param argv : arguments (device name for example)
 *
 * @return 0 if success, -__LINE__ of error catched else
 */
int do_everything(int argc, LPCWSTR argv[]);

/**
 *  List in stdout all audio output devices detected in computer
 */
std::vector<const char *> list_capture_devices();

/**
 * Capture audio output from device and save it as loopback-capture.wav
 *
 * @param device : Name of the device where to record audio output (if
 *                 device is "d", the program will auto detect where sound comes off,
 *                 but sound must be already playing at program launch, not mandatroy
 *                 if we write a device to listen)
 *
 * @return 0 if record successful, -__LINE__ of error catched else
 */
int capture(LPCWSTR device = L"d");


#endif //RAYMARCHING_MAIN_LOOPBACK_CAPTURE_HPP
